
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-hsbc-obie-SecuredLoans-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-atms-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-branches-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-opendata-CreditCards-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-opendata-current-accounts-1.0.1

docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/obie-opendata-atm-2.2.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/obie-opendata-bca-2.2.1
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/obie-opendata-branch-2.2.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/obie-opendata-ccc-2.2.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/obie-opendata-pca-2.2.1
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/obie-opendata-sme-loan-2.2.0


docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-opendata-MortgageLoans-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-opendata-TimeDepositAccounts-1.0.1
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-opendata-foreign-exchange-rates-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-opendata-personal-foreign-currency-accounts-1.0.0

docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-savings-accounts-1.0.0
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/hk-retail-hsbc-obie-unsecured-loans-1.0.0

docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/cmb-connect-payments-pa-account/
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/cmb-connect-payments-pa-payable/
docker run -it anypipe /bin/bash ./step-publish-and-deploy-proxy.sh ./examples/cmb-connect-payments-pa-payment/