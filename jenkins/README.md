# Jenkins CICD pipeline configuration


## Prerequisites
Jenkins serverr should have docker installed.
Jenkins user should be added to docker runners group:
```shell
sudo usermod -a -G docker jenkins
```

Jekins should have tools preinstalled:

* maven 'Maven'
* jdk 'jdk8'


Anypoint-Pipeline image should be build, see [docker page](../docker) - for more details.

## Sample Jenkins file
Sample Jenkins file contains pipeline definition for Jenkins job.

## Initial Job configuration
In order to configur jenkins job please go to
and fill following properties:

Poll SCM:
```
* * * * *
```

Pipeline
    Definition: Pipeline script from SCM
    Repository: https://github.com/mulesoft-consulting/anypoint-pipeline.git


Branch specifier:
*/features/cloudhub


Script Path:
jenkins/Jenkinsfile

then click Saven and then run your pipeline.
