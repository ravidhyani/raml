# Anypoint Pipeline Scipts

## Requirements

In order to run anypoint scripts it's Linux only supported.

Required tools:

* jq - json query tool, yum install | apt get install 
* yq - yaml query tool, to install:
```shell
wget https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64
chmod u+x yq_linux_amd64
mv ./yq_linux_amd64 /usr/bin/yq
```
* anypoint-cli - installation tools https://docs.mulesoft.com/runtime-manager/anypoint-platform-cli#installation


## Script Files
* applyPolicy.sh - Starting Policy Apply in Anypoint API Manager
* createExchangeAssetPage.sh - Starting Asset Page Creation in Anypoint Exchange
* deployApp.sh - utility script not called directly
* deployProxy.sh - Starting Proxy Deployment in Anypoint Runtime Manager
* importExchangeAssetToAPIManager.sh - Starting Exchange Asset Import to Anypoint API Manager
* uploadExchangeAsset.sh - Starting Asset deployment in Anypoint
* verifyAssetPages.sh - Starting Asset Page verification in Anypoint Exchange


## Pipeline scipts files
Several predefined pipelines are preconfigured to be used as samples:

* step-deploy-app.sh
* step-manage-and-deploy-proxy.sh
* step-publish-and-deploy-app.sh
* step-publish-and-deploy-proxy.sh
* step-publish-to-exchange.sh