source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

function validateTierLimits(){
    local periods="[ms]=milliseconds [sec]=seconds [min]=minutes [hr]=hours [d]=days [wk]=weeks [mo]=months [yr]=years"
    local tier_name=$1
    local curr_limit=$2
    local curr_requests=$(echo "${curr_limit}" | yq -r '.requests // empty')
    if [[ -z ${curr_requests} ]]; then 
        onErrorExit 100 "Config file invalid. One ore more of the limits for SLA '${tier_name}' is missing attribute 'requests'"
    fi 
    local number_re='^[0-9]+$'
    if [[ ! ${curr_requests} =~ $number_re ]]; then
        onErrorExit 100 "Config file invalid. Invalid value (${curr_requests}) for 'requests' in one of the limits of SLA tier '${tier_name}'"
    fi 
    
    local period=$(echo "${curr_limit}" | yq -r '.period // empty')
    
    if [[ -z ${period} ]]; then 
        onErrorExit 100 "Config file invalid. One ore more of the limits for SLA '${tier_name}' lacks attribute 'period'"
    fi 

    if [[ $(echo ${periods} | grep -ic "\[${period}\]") == 0 ]]; then
        onErrorExit 100 "Config file invalid. Period '${period}' for SLA tier '${tier_name}' is invalid. Please specify one of the following: ${periods}"
    fi

}

######################################################################################
# Validate one of the SLA tier defined in the configuration file
#
# Globals:
#   CONFIG_YAML : Will contain the config structure once the 
#   validation hyas been successful
#
# Parameters
#   1: the index of the tier to validate
#
# Return:
#    Error - Exit with an error if the validation 
#    No error - Exit with no error if the configuration file doesn't contain any 
#               SAL tiers configuration
######################################################################################
function validateTier(){
    local idx=$1
    local curr_tier=$(echo "${CONF_YAML}" | yq --argjson num ${idx} '.[$num]')
    local tier_name=$(echo "${curr_tier}" | yq -r '.name // empty')

    if [[ -z ${tier_name} ]]; then 
        onErrorExit 100 "Config file invalid. One or more 'sla-tier's is missing the 'name' attribute."
    fi 
    
    local curr_limits=$(echo "${curr_tier}" | yq '.limits // empty')
    if [[ -z ${curr_limits} ]]; then 
        onErrorExit 100 "Config file invalid. SLA tier '${tier_name}' does not specify any limits"
    fi 

    local num_limits=$(echo "${curr_limits}" | yq '. | length')

    if [[ $(($num_limits)) -eq 0 ]]; then 
        onErrorExit 100 "Config file invalid. SLA tier '${tier_name}' does not specify any limits"
    fi 
    
    #now lets validate the limits
    for ((idx_limit=0;idx_limit<num_limits;idx_limit++)); do
        validateTierLimits $tier_name $(echo "${curr_limits}" | yq -c --argjson num ${idx_limit} '.[$num]')
    done

}

######################################################################################
# Validate the structure of the configuration file
# as far as the SLA tiers configuration is concerned
# It exit or fail if the configuration file is incorrect
# or if no SLA tiers are defined
#
# Globals:
#   CONFIG_YAML : Will contain the config structure once the 
#   validation hyas been successful
#
# Parameters
#   1: the path to the config file
#
# Return:
#    CONFIG_YAML - Set the globa variable CONFIG_YAML to the parsed YAML structure
#    Error - Exit with an error if the validation 
#    No error - Exit with no error if the configuration file doesn't contain any 
#               SAL tiers configuration
######################################################################################
function validateAndBuildYaml {
    local conf_file=$1
    CONF_YAML=$(cat ${conf_file} | yq '."sla-tiers" // empty')
    onErrorExit $? "Error while parsing configuration file ${config_file}. Please check the file is a valid YAML."

    if [[ -z ${CONF_YAML} ]]; then 
        INFO "No SLA tiers specified in the configuration file ${conf_file}. Nothing to do.."
        exit 0
    fi 

    local num_tiers=$(echo "${CONF_YAML}" | yq -r '. | length')
    
    if [[ $((${num_tiers})) -eq 0 ]]; then 
        INFO "No SLA tiers specified in the configuration file ${conf_file}. Nothing to do.."
        exit 0
    fi 

    local num_tiers=$(echo "${CONF_YAML}" | yq '. | length')
    local idx=0
    for ((idx=0;idx<$num_tiers;idx++)); do
        validateTier $idx
    done 
}

function tierExists {
    local existing_tiers=$1
    local TIER_NAME_TO_CHECK=$2
    local found=$(echo $existing_tiers | jq  --arg name "$TIER_NAME_TO_CHECK" '.[] | select(.Name==$name)')
    DEBUG "Checking whether tier already $TIER_NAME_TO_CHECK exists"
    if [[ ! -z "$found" ]]; then 
        DEBUG "Tier $TIER_NAME_TO_CHECK already exists"
        return 1
    else
        DEBUG "Tier '$TIER_NAME_TO_CHECK' does not exist"
        return 0
    fi
}

# Remove any SLA tier that is already applied to the API but that has not been specified in the 
# config file. This will cater for situations where one wants to remove an SLA tier and to do so
# would remove the corresponding configuration from the file.
# NOTE: This operation will fail if the SLA tier already has applications registered against!
function removeStaleSLAs {
    local ID=$1
    local EXISTING_SLAS=$2
    local NEW_SLAS=$(echo "${CONF_YAML}" | yq -r ".[].name")
    DEBUG "Removing existing SLA tiers"
    IFS="|"
    while read -r sla_id sla_name; do
        #Check if this existing SLA is defined in the config file otherwise remove it
        if [[ $(echo $NEW_SLAS | grep -iwc $sla_name) == 0 ]]; then 
            INFO "Existing SLA '$sla_name' has not been specified in the configuration file therefore it will be removed"
            local resp=$(anypoint-cli api-mgr tier delete $APIINSTANCEID $sla_id)
            local status=$?
            DEBUG "Removed stale SAL: $resp"
            onErrorExit $status "Error while removing stale SLA tier '$sla_name'. Error was: $status - code: $status"
            
            if [[ "${resp}" == "Error: This tier still has an associated application." ]]; then
                WARN "SLA tier '$sla_name' could not be removed since there are applications registered against it. Revoke the access to all applications to remove this tier."
            fi
            
            DEBUG "Stale SLA tier '$sla_name' removed : $resp"
        fi

    done < <(echo "$EXISTING_SLAS" | jq --raw-output '.[] | "\(.ID)|\(.Name)"')

    DEBUG "Done removing stale SLA tiers"

}

# buildTierconfig
# This function gradually build the command line or the JSON payload required to create or modify an SLA tier
# Depending on the value passed to "format" it generates a set of command line parameters or a JSON payload
# that will be used to modify (via REST) an existing SLA tier.
function buildTierConfig {
    local currConfig=$1
    local format=$2
    local param=$3
    local value=$4

    if [[ "$param" == "name" ]]; then
        if [[ "$format" == "CLI" ]]; then 
        echo "${currConfig} --name $value"
        else 
            echo "$(echo ${currConfig} | jq --arg name "$value" '.name=$name')"
        fi
    fi

    if [[ "$param" == "a" ]]; then 
        if [[ "$format" == "CLI" ]]; then 
            echo "${currConfig} -a $value"
        else
            echo "$(echo ${currConfig} | jq --arg auto "$value" '.autoApprove=$auto')"
        fi
    fi 

    if [[ "$param" == "l" ]]; then
        if [[ "$format" == "CLI" ]]; then 
            echo "${currConfig} -l $value"
        else
            #Check if the input document already contains a limits element
            if [[ $(echo "${currConfig}" | jq '.limits') == "" ]]; then 
                currConfig=$(echo ${currConfig} | jq '.limits=[]')
            fi 
            local VISIBLE="$(echo ${value} | awk -F , '{print $1}')"
            local REQUESTS="$(echo ${value} | awk -F , '{print $2}')"
            local PERIOD="$(echo ${value} | awk -F , '{print $3}')"
            local MS=0
            case "$PERIOD" in 
                "ms")  
                    let MS=1 
                    ;;
                "sec") 
                    let MS=1000     
                    ;;
                "min") 
                    let MS=1000*60
                    ;;
                "hr")  
                    let MS=1000*60*60
                    ;;
                "d")   
                    let MS=1000*60*60*24
                    ;;
                "wk")  
                    let MS=1000*60*60*24*7
                    ;;
                "mo")  
                    let MS=1000*60*60*24*30
                    ;;
                "yr")  
                    let MS=1000*60*60*24*365
                    ;;
                *)
                    onErrorExit 1 "Invalid time period '$PERIOD' while applying SLA tier"
            esac 

            echo "$(echo ${currConfig} | jq --arg visible $VISIBLE --arg requests $REQUESTS --arg ms $MS '.limits += [{"visible": $visible | test("true"), "maximumRequests": $requests | tonumber, "timePeriodInMilliseconds": $ms | tonumber}]')" 
        fi
    fi

}

# updateTier
# Update an existing SAL tier. This function will use REST to update the tier since at the present, the
# anypoint-cli doesn't provide a command to update a tier
function updateTier {
    local config=$1
    local tier_id=$2
    
    config="$(echo ${config} | jq --arg tierid ${tier_id} '.id=($tierid | tonumber)')"
    config="$(echo ${config} | jq --arg apiId ${APIINSTANCEID} '.apiId=$apiId')"
    config="$(echo ${config} | jq --arg orgId ${ORG_ID} '.organizationId=$orgId')"
    config="$(echo ${config} | jq '.status="ACTIVE"')"
    config="$(echo ${config} | jq -c)"
    DEBUG "Updating tier using the following config: ${config}"
    
    local debug_curl="-sS -o /dev/null"
    local output_pipe=" 2>/dev/null > /tmp/nix.html"

    if [ ! -z $ANYPOINT_CLI_SH_TRACING ]; then
        debug_curl="-vvv"
        output_pipe=""
    fi 

    local curl_cmd="curl -k -X PUT $debug_curl -H 'Content-Type: application/json' -H 'Authorization: Bearer ${AUTH_TOKEN}' -d '"${config}"' \"https://$ANYPOINT_HOST/apimanager/api/v1/organizations/${ORG_ID}/environments/${ENV_ID}/apis/${APIINSTANCEID}/tiers/${tier_id}\" ${output_pipe}"

    TRACE "CURL COMMAND: $curl_cmd"

    eval $curl_cmd
}

# applySLA
# Apply SLA tiers as specified in the configuration file passed as parameter
# PARAMETERS:
# 1: Full path of the yml configuration file
# 2: Instance ID of the API to which to apply the SLA tiers.
# 3: JSON with the existing SLA tiers as returned by running "anypoint-cli api-mgr tiers list $MULE_APIINSTANCEID"
# IMPORTANT: Due to a limitation of the anypoint-cli (as of version 3.0.5) it is not possible to
# modify an SLA tier therefore any change required once the tier has been created must be performed
# manually.
# CONFIGURATION EXAMPLE :
# sla-tiers:
# - name: gold
#   autoapprove: false #whether or not any request to join this SLA will be approved automatically
#   limits: 
#   - visible: true #whether or not this limit should be visible
#     requests: 100 #number of requests allowed per 'period'
#     period: secs #period can be: ms(millisecond), sec(second),  min(minute), hr(hour), d(day), wk(week), mo(month) or yr(year).
#   - visible: true
#     requests: 1000
#     period: min
# - name: free
#   autoapprove: true
#   limits:
#     - visible: true
#       requests: 10
#       period: sec
#     - visible: true
#       requests: 100
#       period: miN 

function applySLA {
    local periods="[ms]=milliseconds [sec]=seconds [min]=minutes [hr]=hours [d]=days [wk]=weeks [mo]=months [yr]=years"
    local existing_tiers=$1
    local num_slas=$(echo "${CONF_YAML}" | yq -r ". | length")
    DEBUG "Applying SLA tiers defined in the API configuration file" 
    local curr_sla_idx=0

    declare -a COMMANDS

    while [[ curr_sla_idx -lt $num_slas ]];
    do
        local CURR_SLA=$(echo "${CONF_YAML}" | yq --argjson num ${curr_sla_idx} '.[$num]')
        local NEW_TIER_NAME=$(echo $CURR_SLA | jq -r '.name')
        local autoapprove=$(echo $CURR_SLA | jq '.autoapprove // empty')
        local limits=$(echo $CURR_SLA | jq '.limits')

        local TIER_EXISTS
        tierExists "$existing_tiers" "$NEW_TIER_NAME"
        TIER_EXISTS=$?

        local FORMAT
        local options 
        local TIER_ID
        if [[  $TIER_EXISTS == 1 ]]; then
            DEBUG "Updating tier '$NEW_TIER_NAME'"
            FORMAT="JSON"
            options="{}"
            TIER_ID=$(echo $existing_tiers | jq  --arg name "$NEW_TIER_NAME" '.[] | select(.Name==$name) | .ID')
        else 
            DEBUG "Creating new SLA tier $NEW_TIER_NAME"
            FORMAT="CLI"
            options=""
        fi
        
        
        options=$(buildTierConfig "$options" "$FORMAT" "name" "$NEW_TIER_NAME")
        if [[ ! -z "${autoapprove}" ]] && [[ $(echo "${autoapprove}" | tr '[:upper:]' '[:lower:]' | tr -d ' ') == "true" ]]; then
            options=$(buildTierConfig "$options" "$FORMAT" "a" "true")
        else 
            options=$(buildTierConfig "$options" "$FORMAT" "a" "false")
        fi 
        
        local limits_count=$(echo "${limits}" | jq '. | length')
        local curr_limit_num=0
        while [[ curr_limit_num -lt ${limits_count} ]];
        do
            local CURR_LIMIT=$(echo $limits | jq --argjson num $curr_limit_num '.[$num]')
            local VISIBLE="$(echo $CURR_LIMIT | jq -r '.visible')" 
            local REQUESTS="$(echo $CURR_LIMIT | jq -r '.requests')" 
            local PERIOD="$(echo $CURR_LIMIT | jq -r '.period' | tr '[:upper:]' '[:lower:]')" 
            if [[ $(echo $periods | grep -ic "\[$PERIOD\]") == 0 ]]; then
                onErrorExit 1 "Error while applying SAL tier. Period '$PERIOD' for SLA '$NEW_TIER_NAME' is invalid. Please specify one of $periods"
            fi
            options=$(buildTierConfig "$options" "$FORMAT" "l" "$VISIBLE,$REQUESTS,$PERIOD")

            let curr_limit_num=curr_limit_num+1
        done 
        
        local STATUS
        if [[ $TIER_EXISTS == 1 ]]; then # Tier exists: Update tier (using REST)
            INFO "Updating SLA tier '$NEW_TIER_NAME'"
            updateTier "$options" $TIER_ID
            STATUS=$?
            if [[ $STATUS==0 ]]; then
                INFO "SLA tier updated."
            fi
        else # Tier does not exist: Create new tier
            local cmd="anypoint-cli api-mgr tier add $options $APIINSTANCEID" 
            DEBUG "Adding tier running the following CLI command: $cmd"
            INFO "Adding new tier:  $cmd"
            eval $cmd
            STATUS=$?
            if [[ $STATUS == 0 ]]; then 
                INFO "New SLA tier '$NEW_TIER_NAME' successfully created"
            fi
        fi

        onErrorExit $STATUS "Error while applying SLA tier '$NEW_TIER_NAME'..."
        let curr_sla_idx=curr_sla_idx+1

    done

}

function mainSLA(){
    INFO "Applying SLA tiers.."

    VARS=( "ANYPOINT_HOST" "ANYPOINT_USERNAME" "ANYPOINT_PASSWORD" "ANYPOINT_ORG" "ANYPOINT_ENV" "APIINSTANCEID")

    checkEnvVariables VARS

    if [[ -z "${MULE_CONFIG_FILE_PATH}" ]]; then
        INFO "Variable MULE_CONFIG_FILE_PATH not set. No SLA will be applied to API ${APIINSTANCEID}"
        exit 0
    fi 

    if [[ ! -z "${MULE_CONFIG_FILE_PATH}" ]] &&  [[ ! -f "${MULE_CONFIG_FILE_PATH}" ]]; then
        onErrorExit 1 "Configuration file ${MULE_CONFIG_FILE_PATH} could not be found!"
    fi


    EXISTING_SLAS="$(anypoint-cli api-mgr tier list ${APIINSTANCEID} -o json)"
    declare CONF_YAML
    validateAndBuildYaml ${MULE_CONFIG_FILE_PATH}
    
    #Set global variables
    ORG_ID=$(getOrgId $ANYPOINT_ORG)
    ENV_ID=$(getEnvId $ANYPOINT_ENV)
    AUTH_TOKEN=$(getAuthToken)

    #Only remove stale SLA if specifically told. 
    #It is advised not to set this flag or set it to false
    if [[ ! -z "${MULE_REMOVE_STALE_SLA}" ]] && [[ $(echo ${MULE_REMOVE_STALE_SLA} | tr '[:upper:]' '[:lower:]' | tr -d ' ') == "true" ]]; then
        removeStaleSLAs ${APIINSTANCEID} "${EXISTING_SLAS}"
    fi 
    applySLA "${EXISTING_SLAS}"
}

#load main unless using bats
if [ -z ${BATS_TEST_DIRNAME} ]; then
  mainSLA
fi