echo Starting Policy Apply in Anypoint API Manager...

API_PATH=$1
source $API_PATH/config.sh

source ./common.sh

VARS=( "ANYPOINT_HOST" "ANYPOINT_USERNAME" "ANYPOINT_PASSWORD" "ANYPOINT_ORG" "ANYPOINT_ENV" "MULE_POLICYFILESPATH" "MULE_REMOVE_STALE_POLICIES" )

checkEnvVariables VARS

function getPolicyTemplates {
	# NODE_TLS_REJECT_UNAUTHORIZED=0 is necessary to avoid issue with the cli when invoking graphql APIs
	TEMPLATES=$(NODE_TLS_REJECT_UNAUTHORIZED=0  anypoint-cli api-mgr policy list -m)
	onErrorExit $? "Error while retrieving list of policies in org $ANYPOINT_ORG: $TEMPLATES"
		
	echo "$TEMPLATES" | tail  -n +2
}

#Don't do anything if there are no policies to be applied
if [ "$(find $MULE_POLICYFILESPATH -iname '*.json')" ]; then
	INFO "Going to apply the following policies: \n$(find $MULE_POLICYFILESPATH -iname '*.json')"
else 
	INFO "No policy configuration files found in $MULE_POLICYFILESPATH. Skipping policies application."
	exit 0
fi

#Hack to get API Instance Id - jq tool required
APIINSTANCEID=$(anypoint-cli api-mgr api list --output json --assetId $MULE_ASSETIDENTIFIER --apiVersion $MULE_APIVERSION | jq '.[0]."Instance ID"')
POLICY_TEMPLATES=$(getPolicyTemplates)
DEBUG "The policy templates $POLICY_TEMPLATES"
declare -A POLICYASSETIDTOPOLICYIDMAP	#Map to Policy Asset Id and Policy Id
DEBUG "Retrieving api $APIINSTANCEID's list of policies"
POLICYLIST=$(anypoint-cli api-mgr policy list $APIINSTANCEID -m)
onErrorExit $? "Error while retrieving policies applied to API $APIINSTANCEID : $POLICYLIST"
#POLICYLIST=$(echo $POLICYLIST | grep "\\n")
DEBUG "Policies currently applied to API $APIINSTANCEID: $POLICYLIST"
{
	read
	read
	i=0
	while read -r line
	do
		CURR_POLICY_ID=$(echo "$line" | sed 's/\x1b\[[0-9;]*m//g' | awk '{print $3}')
		APPLIEDPOLICYIDS[$i]="$CURR_POLICY_ID"
		POLICYASSETIDTOPOLICYIDMAP[$CURR_POLICY_ID]=$(echo "$line" | sed 's/\x1b\[[0-9;]*m//g' | awk '{print $1}')
		((i++))
	done
}	< <(printf '%s\n' "$POLICYLIST")
DEBUG "Asset IDs of all policies already applied to api $APIINSTANCEID : ${APPLIEDPOLICYIDS[*]}"

i=0
for FILE in $MULE_POLICYFILESPATH/*;	#Update an existing policy or apply new policy
do	
	POLICYFILENAME=$(echo ${FILE##*/})
	POLICYFILENAMEWITHOUTEXTENSION=$(echo "${POLICYFILENAME%.*}")
	POLICYASSETID=$(echo $POLICYFILENAMEWITHOUTEXTENSION | cut -d'_' -f 1)
	POLICYID=${POLICYASSETIDTOPOLICYIDMAP[${POLICYASSETID}]}
	POLICYIDSTOBEAPPLIED[$i]=$POLICYASSETID
	((i++))
	POLICYVERSION=$(echo $POLICYFILENAMEWITHOUTEXTENSION | cut -d'_' -f 2)
	EXISTINGPOLICY=false
	CONFIGJSON=$(cat $FILE)
	INFO "Applying policy $POLICYASSETID (v$POLICYVERSION)"
	for APPLIEDPOLICYID in "${APPLIEDPOLICYIDS[@]}"
	do
		DEBUG "Check that '$POLICYASSETID' == '$APPLIEDPOLICYID'"
		if [ "$POLICYASSETID" == "$APPLIEDPOLICYID" ]; then	#Update the existing Policy in API Manager
			EXISTINGPOLICY=true
			INFO "Policy $POLICYFILENAMEWITHOUTEXTENSION already applied to API $APIINSTANCEID . Policy will be updated"
			INFO "Applying policy configuration : $CONFIGJSON"
			RESPONSE=$(anypoint-cli api-mgr policy edit $APIINSTANCEID $POLICYID --config $CONFIGJSON)
			onErrorExit $? "$RESPONSE"
			INFO "Policy $POLICYASSETID (v$POLICYVERSION) updated : $RESPONSE"
			break
		fi
	done
	
	if [ "$EXISTINGPOLICY" = false ]; then	#Apply the new Policy in API Manager
		INFO "About to apply new policy $POLICYASSETID (v$POLICYVERSION)"
		GROUPID=$(echo "$POLICY_TEMPLATES" | grep $POLICYASSETID | grep $POLICYVERSION | sed 's/\x1b\[[0-9;]*m//g' | awk '{print $1}')
		GROUPID=$(echo $GROUPID | sed 's/\\u001b\[[0-9]*m//g')
		if [ ! "${GROUPID}" ]; then
		   onErrorExit 1 "Unable to find policy $POLICYASSETID (v$POLICYVERSION) while applying policy to API with ID $APIINSTANCEID"
		#else 
		#	DEBUG "The GroupID: '$GROUPID'"
		fi 

		INFO "Applying policy configuration : $CONFIGJSON"
		RESPONSE=$(anypoint-cli api-mgr policy apply --policyVersion $POLICYVERSION --config $CONFIGJSON --groupId $GROUPID $APIINSTANCEID $POLICYASSETID)

		onErrorExit $? "$RESPONSE"
		INFO "Policy $POLICYASSETID (v$POLICYVERSION) successfully 	applied. Response was: $RESPONSE"
	fi
		
	
done

if [ "$MULE_REMOVE_STALE_POLICIES" = true ]; then
	#Getting the applied policies minus policies to be applied. The difference will be removed. 
	POLICIESTOBEREMOVED=(`echo ${APPLIEDPOLICYIDS[@]} ${POLICYIDSTOBEAPPLIED[@]} ${POLICYIDSTOBEAPPLIED[@]} | tr ' ' '\n' | sort | uniq -u | sed 's/\x1b\[[0-9;]*m//g' `)

	for POLICYTOBEREMOVED in "${POLICIESTOBEREMOVED[@]}"
	do
		POLICYIDTOBEREMOVED=${POLICYASSETIDTOPOLICYIDMAP[${POLICYTOBEREMOVED}]}
		INFO "Policy $POLICYTOBEREMOVED will be removed from API $APIINSTANCEID"
		RESPONSE=$(anypoint-cli api-mgr policy remove $APIINSTANCEID $POLICYIDTOBEREMOVED)
		onErrorExit $? "$RESPONSE"
		INFO "Policy removal performed with response '$RESPONSE'"
		
	done
fi
status=$?
if [ $status -gt 0 ]; then
	echo Error: $RESPONSE 
else
	echo $RESPONSE
fi 


