source ./common.sh

# checkAppStatus
# Continuously poll for the app status until the status is either STARTED (successfull) or FAILED (unsuccessful)
# If the status if FAILED this function will exit with an error message
function checkAppStatus { 
    local sleepTimeSeconds=5
    local maxPollingIterations=12
    local maxPollingTime=$(($sleepTimeSeconds*$maxPollingIterations))
    local appName=$1
    local target=$2
    local currIteration=${3:-0}
    local apps=""
    local thisApp=""
    local appId=""
    apps="$(anypoint-cli runtime-mgr standalone-application list | sed 's/\x1b\[[0-9;]*m//g')"
    thisApp="$(echo "$apps" | grep "$appName" | grep "$target")"
    appId="$(echo "$thisApp" | awk '{print $1}')"
    DEBUG "Polling the status of app  '$appName (id: $appId)'"
    appStatus="$( anypoint-cli runtime-mgr standalone-application describe-json "$appId" | jq -r .lastReportedStatus)"
    DEBUG "Polling $currIteration time out of $maxPollingIterations" 
    DEBUG "Checking status of app '$appName' (id: $appId)" 
    DEBUG "Current application status: $appStatus"

    if [ -z "$appStatus" ]; then
        onErrorExit 1 "Unable to determine the application status. Application status is empty!"
    elif [ "$appStatus" == "DEPLOYMENT_FAILED" ]; then 
        onErrorExit 1 "Application '$appName' (id: $appId) failed to start. Please check in Anypoint Runtime Manager the reason why the application deployment failed."
    elif [ "$appStatus" == "STARTED" ]; then 
        DEBUG "Application '$appName' (id: $appId) successfully deployed and started."
        return 0
    else
        if [ $currIteration == $maxPollingIterations ]; then
            onErrorExit 1 "After polling for $maxPollingTime seconds, the application '$appName' neither started or failed to deploy on server '$target'. Aborting..."
        else
            DEBUG "Application not yet started. Sleeping for $sleepTimeSeconds seconds"
            sleep $sleepTimeSeconds
            DEBUG "Slept. Now checking the app status again..."
            checkAppStatus "$appName" "$target" $((currIteration+1))
        fi
    fi
}

function validateAppName {
    local appName=$1
    DEBUG "Validating application name '$appName'"
    local length=$(echo "$appName" | awk '{print length}')
    if [[ "$appName" == -* ]] || [[ "$appName" == *- ]]; then
        onErrorExit 1 "Application name '$appName' is invalid. Name cannot start or finish with a dash."
    fi 

    if [[ length -gt 42 ]]; then 
        onErrorExit 1 "Application name '$appName' must be less than 42 characters long. Lenght is $length"
    fi 

    if [[ length -lt 3 ]]; then 
        onErrorExit 1 "Application name '$appName' must be more than 3 characters long. Lenght is $length"
    fi 
    
    if [[ ! "$appName" =~ ^[A-Za-z0-9-]+$ ]]; then
        onErrorExit 1 "Application name '$appName' invalid. It can only contain numbers, letters and the '-' symbol."
    fi 

    DEBUG "Application name '$appName' valid"
    
}

INFO "Deployment of Mulesoft application"

VARS=( "ANYPOINT_HOST" "ANYPOINT_USERNAME" "ANYPOINT_PASSWORD" "ANYPOINT_ORG" "ANYPOINT_ENV" "MULE_RUNTIMETARGETS" "MULE_JAR_APP_PATH" "MULE_APP_NAME")

checkEnvVariables VARS


if [ ! -f $MULE_JAR_APP_PATH ]; then
    onErrorExit 1 "Application file $MULE_APP_JAR could not be found."
fi

validateAppName "$MULE_APP_NAME"

INFO "Deploying application $MULE_APP_NAME (jar location $MULE_APP_JAR) to the following servers: $MULE_RUNTIMETARGETS" 
Check runtimes exists
deployedApplications=$(anypoint-cli runtime-mgr standalone-application list | sed 's/\x1b\[[0-9;]*m//g' | grep "$MULE_APP_NAME")
deployedApplicationsCount=$(echo -n "$deployedApplications" | grep -c '^')
declare -A serversToAppIDMap	#Map to server name and ARM appId. API Id needed to redeploy a proxy

if [ $deployedApplicationsCount -gt 0 ]; then	
	{
		server=0
		while read -r line
		do
			serversDeployedTo[$server]=$(echo "$line" | sed 's/\x1b\[[0-9;]*m//g' | awk '{print $4}')
			serversToAppIDMap[$(echo "$line" | sed 's/\x1b\[[0-9;]*m//g' | awk '{print $4}')]=$(echo "$line" | sed 's/\x1b\[[0-9;]*m//g' | awk '{print $1}')
			((server++))
		done
	}	< <(printf '%s\n' "$deployedApplications")
fi	
	

IFS=","
for target in $MULE_RUNTIMETARGETS
do
	ALREADY_DEPLOYED=false
	for serverDeployedTo in "${serversDeployedTo[@]}"
	do
		if [ $target ==  $serverDeployedTo ]; then #Re-deploy already deployed application
			ALREADY_DEPLOYED=true
			appId=${serversToAppIDMap[${serverDeployedTo}]}
            INFO "Application '$MULE_APP_NAME' already deployed (id: $appId) on target '$target'. Redeploying..."
			RESPONSE=$(anypoint-cli runtime-mgr standalone-application modify $appId $MULE_JAR_APP_PATH)
			status=$?
			#Exit as soon as a deployment fails. This is to prevent to deploy an entire stripe of failed proxies if we're targeting several clusters
			onErrorExit $status "Error while updating proxy $MULE_APP_NAME with Runtime Manager Id: $appId to target $target. Error message: $ $RESPONSE "
			INFO "Successfully re-deployed application '$MULE_APP_NAME' (id: $appId) to target $target"
			INFO "Waiting for application to start on target $target..."
			#TODO ADD POLLING TO CHECK THE PROXY HAS BEEN DEPLOYED.. ONLY THEN CARRY ON
            checkAppStatus $MULE_APP_NAME $target
            INFO "Application successfully deployed and started!"
			break
		fi		
	done
	if [ "$ALREADY_DEPLOYED" = false ]; then	#Deploy application new target
        INFO "Deploying new application '$MULE_APP_NAME' to $target"
		RESPONSE=$(anypoint-cli runtime-mgr standalone-application deploy $target $MULE_APP_NAME $MULE_JAR_APP_PATH)
		status=$?
		onErrorExit $status "Error while deploying proxy $MULE_APP_NAME ito target $target. Error message: $ $RESPONSE"
		INFO "Application $MULE_APP_NAME successfully uploaded and queued for deployment to target $target. Response from the server: $RESPONSE" 
        INFO "Waiting for application '$MULE_APP_NAME' to start on target $target..."
		#TODO ADD POLLING TO CHECK THE PROXY HAS BEEN DEPLOYED.. ONLY THEN CARRY ON
        checkAppStatus $MULE_APP_NAME $target
        INFO "Application '$MULE_APP_NAME' successfully deployed and started."
	fi
done