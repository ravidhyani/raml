#!/bin/bash

#SET SOME DEFAULTS
export MULE_CLASSIFIER="raml" #since mule apps only use ramls

./uploadExchangeAsset.sh 

./createExchangeAssetPage.sh

./verifyAssetPages.sh

./importExchangeAssetToAPIManager.sh

./applyPolicy.sh

./deployApp.sh
