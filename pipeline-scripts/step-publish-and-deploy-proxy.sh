#!/bin/bash
API_PATH=$1

#SET SOME DEFAULTS
export MULE_CLASSIFIER="raml" #since mule apps only use ramls

source $API_PATH/config.sh

./uploadExchangeAsset.sh 

./createExchangeAssetPage.sh

./verifyAssetPages.sh

./importExchangeAssetToAPIManager.sh

./applyPolicy.sh

./deployProxy.sh
